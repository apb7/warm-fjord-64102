# importing modules
import sys
import os
import json
import subprocess

sample_data = json.load(open('sample_data.json', 'r'))
states = json.load(open('state_data.json', 'r'))
headers = json.load(open('headers.json', 'r')).get('entries')

def average(head, district_list, t='mean'):
    s = 0
    num = 0
    for district in district_list:
        x = district
        #print(x)
        try:
            s += float(x.get('Total')[0].get(head)) 
            num += 1
        except:
            pass
    if (num>0):
        if(t=='sum'):
            return str("{0}".format(str(int(s))))
        if(t=='mean'):
            return str("{0:.2f}".format(s/num))
    else:
        return 'N/A'


def output_state(state_data, path):
    #print(state_data)
    try:
        os.makedirs(path)
    except:
        pass
    district_set = set()    # set always maintains unique entries

    for district in state_data:
        district_set.add(district.get('District').strip())
    #print(district_set)

    district_list = []

    for district in district_set:
        district_array = list(filter(lambda x: x.get('District').strip() == district, state_data))
        district_urban = list(filter(lambda x: x.get('Tier').strip() == 'Urban', district_array))
        district_rural = list(filter(lambda x: x.get('Tier').strip() == 'Rural', district_array))
        district_total = list(filter(lambda x: x.get('Tier').strip() == 'Total', district_array))

        district_list.append(output_district(district_urban, district_rural, district_total, path + district + '.json'))

    state_file = open(path + '_state.json', 'w')
    output_dict = {}
    for header in headers:
        head_title = header.get('value')
        evaluation = header.get('evaluation')
        output_dict[head_title]=average(head_title, district_list, evaluation)
    state_file.write(json.dumps(output_dict, indent=2))


def output_district(district_urban, district_rural, district_total, path):
    # TODO merge the 3 into district_data
    district_data = {
        "Urban": district_urban,
        "Rural": district_rural,
        "Total": district_total
    }
    file = open(path, 'w')
    file.write(json.dumps(district_data, indent=2))
    return district_data


def get_state_map():
    state_map = {}
    initial_dir = 'sample_data/'
    for state in os.listdir(initial_dir):
        for district in os.listdir(initial_dir + state):
            if district!='_state.json':
                try:
                    state_map[district[:-5]].append(state)
                except:
                    state_map[district[:-5]] = [state]
    file = open('state_map.json', 'w')
    file.write(json.dumps(state_map, indent=2, sort_keys=True))


def main(initial_path):
    try:
        os.makedirs(initial_path)
    except:
        pass
    for state in states:
        state_data = list(filter(
            lambda x: x.get('State').strip() == state.get('value').strip(), sample_data))
        output_state(state_data, initial_path + state.get('value') + "/")
    get_state_map()

if __name__ == "__main__":
    initial_path = 'sample_data/'
    try:
        # Do not try this at home.
        subprocess.call(['rm', '-rf', initial_path])
    except:
        pass
    main(initial_path)
