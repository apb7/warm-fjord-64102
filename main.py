# -*- coding:utf8 -*-
# !/usr/bin/env python

# TODO add responses for each Intent

import json
import os
import sys

from fuzzywuzzy import fuzz
from flask import Flask, request, make_response, jsonify, render_template

app = Flask(__name__)
log = app.logger

state_map = json.load(open('state_map.json'))
initial_path = 'sample_data/'
threshold = 85

state_list = []
for state in os.listdir(initial_path):
    state_file = open(initial_path + state + '/_state.json')
    state_list.append(json.load(state_file))
    state_list[-1]["state"] = state

district_list = json.load(open('sample_data.json'))

def more(a, b):
    if(a=='N/A' or b=='N/A'):
        return False
    return float(a)>float(b)

def less(a, b):
    if(a=='N/A' or b=='N/A'):
        return False
    return float(a) < float(b)

def compare_state(headers, headers1, comparator):
    return lambda x: eval(str(comparator))(x[headers],x[headers1])

def compare_state_num(headers, num, comparator):
    return lambda x: eval(str(comparator))(x[headers],num)

def compare_district(headers, headers1, comparator):
    return lambda x: eval(str(comparator))(x[headers],x[headers1])

def compare_district_num(headers, num, comparator):
    return lambda x: eval(str(comparator))(x[headers],num)

@app.route('/', methods=['POST'])
def webhook():
    """This method handles the http requests for the Dialogflow webhook.
    This is meant to be used in conjunction with the weather Dialogflow agent.
    """
    req = request.get_json(silent=True, force=True)
    print(request)
    print(req)
    # Each intent has an associated action. Since we have
    # 5 possible intents, there are 5 possible actions:
    # input.welcome, input.unknown, comparator, comparatorDistricts,
    # comparatorStates, genHeaders, genOpsDistrict
    # and genOpsState.
    try:
        action = req.get('queryResult').get('action')
    except AttributeError:
        return 'ERROR: No action found.'

    # input.welcome and input.unknown are handled by dialogFlow
    # itself. We need to set the response for the remaining six actions.
    if action == 'genOpsDistrict':
        res = gen_ops_district(req)
    elif action == 'genOpsState':
        res = gen_ops_state(req)
    elif action == 'genHeaders':
        res = gen_headers(req)
    elif action == 'comparator':
        res = comparator(req)
    elif action == 'comparatorStates':
        res = comparator_states(req)
    elif action == 'comparatorDistricts':
        res = comparator_districts(req)
    else:
        log.error('Unexpected action: ' + str(action))

    print('Action: ' + str(action))
    print('Response: ' + str(res))

    return make_response(jsonify({'fulfillmentText': res}))


@app.route('/', methods=['GET'])
def webagent():
    return make_response(render_template('index.html'), 200)


def gen_ops_district(req):
    params = req.get('queryResult').get('parameters')
    operators = params.get('operators')
    headers = params.get('headers')
    district = params.get('district').upper()
    tier = params.get('tier')
    district_info = get_district_info(district, headers, tier=tier)

    return district_info


def gen_ops_state(req):
    params = req.get('queryResult').get('parameters')
    operators = params.get('operators')
    headers = params.get('headers')
    state = params.get('state').upper()
    if state == 'ALL':
        state_infos = ''
        for root, dirs, _ in os.walk(initial_path):
            if root == initial_path:
                for state_dir in sorted(dirs):
                    state_infos += get_state_info(state_dir, headers)
                return state_infos
    else:
        state_info = get_state_info(state, headers)
        return state_info


def gen_headers(req):
    params = req.get('queryResult').get('parameters')
    headers = params.get('headers')
    state_infos = ''
    for root, dirs, _ in os.walk(initial_path):
        if root == initial_path:
            for state_dir in sorted(dirs):
                state_infos += get_state_info(state_dir, headers)
            return state_infos


def comparator(req):
    params = req.get('queryResult').get('parameters')
    comparators = params.get('comparators')
    headers = params.get('headers')
    headers1 = params.get('headers1')
    geoarea = params.get('geoarea')
    number = params.get('number')

    # if `state` condition : districts in rajasthan with ....

    if(headers1):
        if(geoarea=='state'):
            res=list(filter(compare_state(headers, headers1, comparators), state_list))
            res = [x["state"] for x in res ]
            res="\n".join(res)
        elif(geoarea=='district'):
            res=list(filter(compare_district(headers, headers1, comparators), district_list))
            res = [x["District"] for x in res ]
            res="\n".join(res)

    elif(number):
        if(geoarea=='state'):
            res=list(filter(compare_state_num(headers, number , comparators), state_list))
            res = [x["state"] for x in res ]
            res="\n".join(res)
        elif(geoarea=='district'):
            res=list(filter(compare_district_num(headers, number , comparators), district_list))
            res = [x["District"] for x in res ]
            res="\n".join(res)
    else:
        res = "Sorry I didn't get that!"

    return res


def comparator_states(req):
    params = req.get('queryResult').get('parameters')
    comparators = params.get('comparators')
    headers = params.get('headers')
    state1 = params.get('state1')
    state2 = params.get('state2')

    # TODO: Implement logical comparators.
    state1_info = get_state_info(state1, headers)
    state2_info = get_state_info(state2, headers)
    res = state1_info + state2_info

    return res


def comparator_districts(req):
    params = req.get('queryResult').get('parameters')
    comparators = params.get('comparators')
    headers = params.get('headers')
    district1 = params.get('district1')
    district2 = params.get('district2')

    # TODO: Implement logical comparators.
    district1_info = get_district_info(district1, headers)
    district2_info = get_district_info(district2, headers)
    res = district1_info + district2_info

    return res


def fuzzy_search(a, b):
    fuzz_ratio = fuzz.partial_ratio(a, b)
    return fuzz_ratio > threshold


def get_state_info(state, headers):
    try:
        state_data = json.load(open(initial_path + state + '/' + '_state.json'))
        try:
            state_info = state + ': ' + state_data[headers] + '\n'
        except KeyError:
            return (
                'There is no such header ' + headers +
                ' Try something else maybe?')
    except:
        return (
            'There is no such state ' + state +
            ' Did you mean ' +
            ', '.join(list(filter(lambda x: fuzzy_search(x, state), os.listdir(initial_path)))))

    return state_info


def get_district_info(district, headers, tier='Total'):
    try:
        states_corres_district = state_map[district]
    except KeyError:
        return (
            'There is no such district ' + district +
            ' Did you mean ' +
            ', '.join(list(filter(lambda x: fuzzy_search(x, district), state_map.keys()))))

    for state in states_corres_district:
        district_data = json.load(open(initial_path + state + '/' + district + '.json'))
        district_tier = district_data[tier][0]
        try:
            district_info =  district + ': ' + str(district_tier[headers]) + '\n'

            # Bots should generally give precise information as response.
            # Therefore these lines have been disabled.
            # if tier == 'Total':
            #     district_info += '\n' + 'Rural: ' + str(district_data['Rural'][0][headers])
            #     district_info += '\n' + 'Urban: ' + str(district_data['Urban'][0][headers])
        except KeyError:
            return (
                'There is no such header ' + headers +
                ' Try something else maybe?')

    return district_info


def percentage(resolve):
    pass


if __name__ == "__main__":
    app.run()
